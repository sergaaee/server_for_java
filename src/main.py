import uvicorn

from fastapi import FastAPI
from database import Base, engine

from config import get_settings

from routes import router_users, router_tasks, router_tokens, router_friend
from fastapi.openapi.docs import get_redoc_html
settings = get_settings()

app = FastAPI()



@app.get("/redoc", include_in_schema=False)
async def get_redoc():
    return get_redoc_html(openapi_url="/openapi.json")

app.include_router(router_users)
app.include_router(router_tasks)
app.include_router(router_tokens)
app.include_router(router_friend)

if __name__ == "__main__":

    Base.metadata.create_all(engine)
    uvicorn.run("main:app", host="0.0.0.0", port=9000)
    