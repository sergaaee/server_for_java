FROM python:3.11-alpine
ENV TZ='Israel'

WORKDIR /app/api

COPY requirements.txt /code/requirements.txt


RUN \
 apk add --no-cache openssl openssl-dev postgresql-libs && \
 apk add --no-cache --virtual .build-deps gcc musl-dev postgresql-dev && \
 pip install --no-cache-dir --upgrade -r /code/requirements.txt


COPY ./src/ /app/api/

CMD ["python", "main.py"]

